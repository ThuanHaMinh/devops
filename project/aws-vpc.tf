
locals {
  name   = "ex-${replace(basename(path.cwd), "_", "-")}"
  region = var.regionAWS

  tags = {
    Example    = local.name
    GithubRepo = "terraform-aws-vpc"
    GithubOrg  = "terraform-aws-modules"
  }
}

# resource "aws_internet_gateway" "Myvpc" {
#   count = local.create_vpc && var.create_igw && length(var.public_subnets) > 0 ? 1 : 0

#   vpc_id = local.vpc_id

#   tags = merge(
#     { "Name" = var.name },
#     var.tags,
#     var.igw_tags,
#   )
# }

################################################################################
# VPC Module
################################################################################

module "vpc" {
  source = "./terraform-aws-vpc-master"

  name = local.name
  cidr = "10.0.0.0/16"

  azs                 = ["${local.region}a", "${local.region}b"]
  private_subnets     = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets      = ["10.0.11.0/24", "10.0.12.0/24"]
  database_subnets    = ["10.0.21.0/24", "10.0.22.0/24"]

  create_database_subnet_group = true
  create_database_subnet_route_table = true

  manage_default_network_acl = true
  default_network_acl_tags   = { Name = "${local.name}-default" }

  manage_default_route_table = true
  default_route_table_tags   = { Name = "${local.name}-default" }

  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.name}-default" }

  #VPC DNS Parameter
  enable_dns_hostnames = true
  enable_dns_support   = true

  #NAT gateway outbound communication
  enable_nat_gateway = true
  single_nat_gateway = true
  one_nat_gateway_per_az = true

  public_subnet_tags = {
    Name = "Public-subnets"
  }

  private_subnet_tags = {
    Name = "Private-subnets"
  }
  database_subnet_tags = {
    Name = "Database-subnets"
  }
  vpc_tags = {
    Name = "vpc-dev"
  }
  customer_gateways = {
    IP1 = {
      bgp_asn     = 65112
      ip_address  = "1.2.3.4"
      device_name = "some_name"
    },
    IP2 = {
      bgp_asn    = 65112
      ip_address = "5.6.7.8"
    }
  }

  enable_vpn_gateway = true

  enable_dhcp_options              = true
  dhcp_options_domain_name         = "service.consul"
  dhcp_options_domain_name_servers = ["127.0.0.1", "10.10.0.2"]

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60

  tags = local.tags
}

################################################################################
# Supporting Resources
################################################################################

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group" "vpc_tls" {
  name_prefix = "${local.name}-vpc_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [module.vpc.vpc_cidr_block]
  }

  tags = local.tags
}

