

resource "aws_launch_configuration" "ebs_optimized" {
  name_prefix   = "test_ebs_optimized"
  image_id      = data.aws_ami.amazon_linux.id
  instance_type = var.supported_type
  ebs_optimized = module.supported_ebs.answer
}
resource "aws_instance" "ebs_optimized" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.supported_type
  ebs_optimized = module.supported_ebs.answer
  tags          = merge(local.tags, { "Name" = "test_ebs_optimized" })
}
module "supported_ebs" {
  source        = "./terraform-aws-ebs-optimized-master"
  instance_type = var.supported_type
}
module "unknown_type" {
  source        = "./terraform-aws-ebs-optimized-master"
  instance_type = "z1.whatever"
}