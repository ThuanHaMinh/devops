resource "aws_route53_resolver_rule" "sys" {
  domain_name = "sys-example.com"
  rule_type   = "SYSTEM"
}

resource "aws_route53_resolver_rule_association" "example" {
  resolver_rule_id = aws_route53_resolver_rule.sys.id
  vpc_id           = module.vpc.vpc_id
}

module "zones" {
  source = "./terraform-aws-route53-master/modules/zones"

  zones = {
    "terraform-aws-modules-example.com" = {
      comment = "terraform-aws-modules-example.com (production)"
      tags = {
        Name = "terraform-aws-modules-example.com"
      }
    }

    "app.terraform-aws-modules-example.com" = {
      comment           = "app.terraform-aws-modules-example.com"
      delegation_set_id = module.delegation_sets.route53_delegation_set_id.main
      tags = {
        Name = "app.terraform-aws-modules-example.com"
      }
    }

    "private-vpc.terraform-aws-modules-example.com" = {
      # in case than private and public zones with the same domain name
      domain_name = "terraform-aws-modules-example.com"
      comment     = "private-vpc.terraform-aws-modules-example.com"
      vpc = [
        {
          vpc_id = module.vpc.vpc_id
        }
      ]
      tags = {
        Name = "private-vpc.terraform-aws-modules-example.com"
      }
    }
  }

  tags = {
    ManagedBy = "Terraform"
  }
}
module "delegation_sets" {
  source = "./terraform-aws-route53-master/modules/delegation-sets"

  delegation_sets = {
    main = {}
  }
}
module "resolver_rule_associations" {
  source = "./terraform-aws-route53-master/modules/resolver-rule-associations"

  vpc_id = module.vpc.vpc_id

  resolver_rule_associations = {
    example = {
      resolver_rule_id = aws_route53_resolver_rule.sys.id
    },
    example2 = {
      name             = "example2"
      resolver_rule_id = aws_route53_resolver_rule.sys.id
      vpc_id           = module.vpc.vpc_id
    },
  }
}

# module "s3_bucket" {
#   source = "terraform-aws-modules/s3-bucket/aws"

#   bucket_prefix = "s3-bucket-"
#   force_destroy = true

#   website = {
#     index_document = "index.html"
#   }
# }

# module "cloudfront" {
#   source = "terraform-aws-modules/cloudfront/aws"

#   enabled             = true
#   wait_for_deployment = false

#   origin = {
#     s3_bucket = {
#       domain_name = module.s3_bucket.s3_bucket_bucket_regional_domain_name
#     }
#   }

#   default_cache_behavior = {
#     target_origin_id       = "s3_bucket"
#     viewer_protocol_policy = "allow-all"
#   }

#   viewer_certificate = {
#     cloudfront_default_certificate = true
#   }
# }
